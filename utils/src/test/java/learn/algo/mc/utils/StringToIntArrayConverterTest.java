package learn.algo.mc.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringToIntArrayConverterTest {

    @Test
    void convert() {
        StringToIntArrayConverter converter = new StringToIntArrayConverter();
        Object source = "1;2;3";
        Object result = converter.convert(source, int[].class);
        assertTrue(int[].class.isAssignableFrom(result.getClass()));
        int[] array = (int[]) result;
        int[] expected = {1, 2, 3};
        assertArrayEquals(expected, array);
    }
}
