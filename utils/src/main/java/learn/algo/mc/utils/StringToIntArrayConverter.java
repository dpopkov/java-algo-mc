package learn.algo.mc.utils;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

/**
 * Converter can be used for parameterized tests.
 * <br/>
 * Example:
 * <pre>
 * &#064;ParameterizedTest
 * &#064;CsvSource({ "1;2;3;4" })
 * void someRecursive(
 *      &#064;ConvertWith(StringToIntArrayConverter.class) int[] arr
 * ) { }
 * </pre>
 */
public class StringToIntArrayConverter extends SimpleArgumentConverter {

    private static final String REGEX = "\\s*;\\s*";

    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
        if (source instanceof String && int[].class.isAssignableFrom(targetType)) {
            final String[] tokens = ((String) source).split(REGEX);
            final int[] array = new int[tokens.length];
            for (int i = 0; i < tokens.length; i++) {
                array[i] = Integer.parseInt(tokens[i]);
            }
            return array;
        } else {
            throw new IllegalArgumentException("Conversion from " + source.getClass() + " to "
                    + targetType + " not supported.");
        }
    }
}
