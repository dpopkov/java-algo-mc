package learn.algo.mc.s07arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class P012ContainsDuplicateLC0217Test {

    @Test
    void testContainsDuplicate() {
        int[] a = {1, 2, 3, 1};
        assertTrue(P012ContainsDuplicateLC0217.containsDuplicate(a));
        int[] b = {1, 2, 3, 4};
        assertFalse(P012ContainsDuplicateLC0217.containsDuplicate(b));
        int[] c = {1,1,1,3,3,4,3,2,4,2};
        assertTrue(P012ContainsDuplicateLC0217.containsDuplicate(c));
    }
}
