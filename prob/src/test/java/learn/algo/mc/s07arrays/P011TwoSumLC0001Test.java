package learn.algo.mc.s07arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class P011TwoSumLC0001Test {

    @Test
    void twoSum() {
        int[] input = {2, 7, 11, 15};
        int[] actual = P011TwoSumLC0001.twoSum(input, 9);
        int[] expected = {0, 1};
        assertArrayEquals(expected, actual);
    }

    @Test
    void twoSumAtEnd() {
        int[] input = {3, 2, 4};
        int[] actual = P011TwoSumLC0001.twoSum(input, 6);
        int[] expected = {1, 2};
        assertArrayEquals(expected, actual);
    }
}
