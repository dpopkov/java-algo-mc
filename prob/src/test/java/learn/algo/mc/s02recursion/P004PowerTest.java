package learn.algo.mc.s02recursion;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class P004PowerTest {

    @ParameterizedTest
    @CsvSource({"2,0,1", "2,3,8", "3,4,81", "-2,3,-8"})
    void calculate(int base, int exponent, int expected) {
        int actual = P004Power.calculate(base, exponent);
        assertEquals(expected, actual);
    }

    @Test
    void calculateNegativeArgument_thenException() {
        assertThrows(IllegalArgumentException.class,
                () -> P004Power.calculate(2, -1));
    }
}
