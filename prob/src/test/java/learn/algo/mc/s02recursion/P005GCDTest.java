package learn.algo.mc.s02recursion;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class P005GCDTest {

    @ParameterizedTest
    @CsvSource({"8,12,4", "24,18,6", "18,24,6", "13,17,1"})
    void calculate(int a, int b, int expected) {
        int actual = P005GCD.calculate(a, b);
        assertEquals(expected, actual);
    }
}
