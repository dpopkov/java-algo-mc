package learn.algo.mc.s02recursion;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class P003SumOfDigitsTest {

    @ParameterizedTest
    @CsvSource({"10,1", "123,6", "12300,6"})
    void calculate(int input, int expected) {
        int actual = P003SumOfDigits.calculate(input);
        assertEquals(expected, actual);
    }

    @Test
    void calculateNegativeArgument_thenException() {
        assertThrows(IllegalArgumentException.class, () -> P003SumOfDigits.calculate(-1));
    }
}
