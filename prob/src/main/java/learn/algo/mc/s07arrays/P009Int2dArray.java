package learn.algo.mc.s07arrays;

/*
Implement two dimensional array.
 */
public class P009Int2dArray {

    static final int EMPTY_CELL_VALUE = Integer.MIN_VALUE;

    public P009Int2dArray(int numRows, int numColumns) {
    }

    public boolean isEmpty(int row, int column) {
        return false;
    }

    public void insert(int row, int column, int value) {
    }

    public int get(int row, int column) {
        return -1;
    }

    public Position search(int value) {
        return null;
    }

    public static class Position {
        public int row;
        public int column;

        public boolean valid() {
            return false;
        }
    }

    public void delete(int row, int column) {
    }
}
