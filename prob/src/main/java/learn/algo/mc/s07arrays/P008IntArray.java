package learn.algo.mc.s07arrays;

import java.util.function.IntConsumer;

public class P008IntArray {
    static final int EMPTY_CELL_VALUE = Integer.MIN_VALUE;

    public P008IntArray(int size) {
    }

    public void set(int index, int value) {
    }

    public int get(int index) {
        return -1;
    }

    public void traverse(IntConsumer consumer) {
    }

    public int find(int value) {
        return -1;
    }

    public void deleteAt(int index) {
    }

    public boolean isEmptyAt(int index) {
        return false;
    }
}
