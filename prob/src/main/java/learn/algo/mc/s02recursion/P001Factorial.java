package learn.algo.mc.s02recursion;

/*
Factorial
- it is the product of all positive integers less than or equal to N.
- Denoted by n!
- Only positive numbers.
- 0! = 1

n! = n * (n - 1) * (n - 2) * ... * 2 * 1

Examples:
    4! = 1*2*3*4 = 24
    10! = 1*2*3*4*5*6*7*8*9*10 = 3_628_800
 */

public class P001Factorial {

    static int calculate(int n) {
        // Implement
        return -1;
    }
}
