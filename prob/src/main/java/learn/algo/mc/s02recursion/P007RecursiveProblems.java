package learn.algo.mc.s02recursion;

public class P007RecursiveProblems {

    /*
    Write a recursive function which accepts a string and returns a new string in reverse.
     */
    static String reverse(String s) {
        // Implement
        return null;
    }

    /*
    Write a recursive function called which returns true if the string passed to it is a palindrome
    (reads the same forward and backward). Otherwise it returns false.
     */
    static boolean isPalindrome(String s) {
        // Implement
        return false;
    }

    /*
    Write a recursive function called someRecursive which accepts an array and a callback.
    The function returns true if a single value in the array returns true when passed to the callback.
    Otherwise it returns false.
     */
    static boolean someRecursive(int[] arr, OddFunction odd) {
        // Implement
        return false;
    }

    static class OddFunction {
        boolean run(int num) {
            return num % 2 != 0;
        }
    }

    /*
    Implement a function that capitalizes each word in String.
    Example
        input: i love java
        output : I Love Java
     */

    static String capitalizeWordForthToBack(String s) {
        // Implement
        return null;
    }

    public static String capitalizeWordBackToForth(String str) {
        // Implement
        return null;
    }
}
