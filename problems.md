## Problems

|       | Title | Difficulty | Problem | Test | Solution |
|-------|-------|------------|---------|------|----------|
|Recursion|     |            |         |      |          |
|P001   | Factorial | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P001Factorial.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P001FactorialTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P001Factorial.java) |
|P002   | Fibonacci | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P002Fibonacci.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P002FibonacciTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P002Fibonacci.java) |
|P003   | Sum of Digits | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P003SumOfDigits.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P003SumOfDigitsTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P003SumOfDigits.java) |
|P004   | Power | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P004Power.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P004PowerTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P004Power.java) |
|P005   | GCD   | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P005GCD.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P005GCDTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P005GCD.java) |
|P006   | Decimal to Binary | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P006DecimalToBinary.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P006DecimalToBinaryTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P006DecimalToBinary.java) |
|P007   | Recursive Problems | easy | [problem](prob/src/main/java/learn/algo/mc/s02recursion/P007RecursiveProblems.java) | [test](prob/src/test/java/learn/algo/mc/s02recursion/P007RecursiveProblemsTest.java) | [solution](sol/src/main/java/learn/algo/mc/s02recursion/P007RecursiveProblems.java) |
|Arrays |     |            |         |      |          |
|P008   | Simple IntArray | easy | [problem](prob/src/main/java/learn/algo/mc/s07arrays/P008IntArray.java) | [test](prob/src/test/java/learn/algo/mc/s07arrays/P008IntArrayTest.java) | [solution](sol/src/main/java/learn/algo/mc/s07arrays/P008IntArray.java) |
|P009   | Simple 2d Array | easy | [problem](prob/src/main/java/learn/algo/mc/s07arrays/P009Int2dArray.java) | [test](prob/src/test/java/learn/algo/mc/s07arrays/P009Int2dArrayTest.java) | [solution](sol/src/main/java/learn/algo/mc/s07arrays/P009Int2dArray.java) |
|P010   | Find Missing Number | easy | [problem](prob/src/main/java/learn/algo/mc/s07arrays/P010MissingNumber.java) | [test](prob/src/test/java/learn/algo/mc/s07arrays/P010MissingNumberTest.java) | [solution](sol/src/main/java/learn/algo/mc/s07arrays/P010MissingNumber.java) |
|P011   | Two Sum (LC1) | easy | [problem](prob/src/main/java/learn/algo/mc/s07arrays/P011TwoSumLC0001.java) | [test](prob/src/test/java/learn/algo/mc/s07arrays/P011TwoSumLC0001Test.java) | [solution](sol/src/main/java/learn/algo/mc/s07arrays/P011TwoSumLC0001.java) |
|P012   | Contains Duplicate (LC217) | easy | [problem](prob/src/main/java/learn/algo/mc/s07arrays/P012ContainsDuplicateLC0217.java) | [test](prob/src/test/java/learn/algo/mc/s07arrays/P012ContainsDuplicateLC0217Test.java) | [solution](sol/src/main/java/learn/algo/mc/s07arrays/P012ContainsDuplicateLC0217.java) |
