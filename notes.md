## Notes about Algorithms

* [Recursion](#recursion)
* [Big O Notation](#big-o-notation)

### Recursion

Recursion: a way of solving a problem by having a function calling itself

* Performing the same operation multiple times with different inputs
* In every step we try smaller inputs to make the problem smaller
* Base condition is needed to stop the recursion, otherwise infinite loop will occur

#### Write Recursion in 3 Steps
Step 1: Identify the Recursive case - the flow
```
    n! = n * (n-1) * (n-2) * ... * 2 * 1
    n! = n * (n-1)!
``` 
```java
static int factorial(int n) {
    return n * factorial(n - 1);
}
```
Step 2: Identify the base case - the stopping criterion
```java
if (n == 1 || n == 0) {
    return 1;
}
```
Step 3: Unintentional case - the constraint
```java
if (n < 0) {
    throw new IllegalArgumentException("Argument must be non-negative");
}
```

[Top](#notes-about-algorithms)

### Big O Notation

Big O is the language and metric we use to describe the efficiency of algorithms.

Например при передаче большого файла по сети время его передачи зависит от размера файла.
Однако при отправке его обычным транспортом (самолетов) время его передачи постоянно и не зависит от размера.

Time Complexity: A way of showing how the runtime of a function increases as the size of input increases.
 
Types of Runtimes: O(N), O(N^2), O(2^N)

#### Types of Complexity
* Big O - it is a complexity that is going to be less or equal to the worst case
* Big Omega - it is a complexity that is going to be at least more than the best case
* Big Theta - it is a complexity that is within bounds of the worst and the best case

#### Most Common Time Complexities

| Complexity    | Name      | Sample                                    |
|---------------|-----------|-------------------------------------------|
| O(1)          | Constant  | Accessing a specific element in array     |
| O(LogN)       | Logarithmic | Find an element in a sorted array       |
| O(N)          | Linear    | Loop through array elements               |
| O(N^2)        | Quadratic | Looking at every index in the array twice |
| O(2^N)        | Exponential | Double recursion in Fibonacci           |

##### O(1) - Constant time
```java
int[] array = {1, 2, 3, 4, 5};
array[0];  // It takes constant time to access the first element
```

##### O(LogN) - Logarithmic time
For example Binary Search Algorithm.

##### O(N) - Linear time
```java
int[] array = {1, 2, 3, 4, 5};
for (int i = 0; i < array.length; i++) {
    System.out.println(array[i]);
}
// Linear time since it is visiting every element in the array
```

##### O(N^2) - Quadratic time
```java
int[] array = {1, 2, 3, 4, 5};
for (int i = 0; i < array.length; i++) {
    for (int j = 0; j < array.length; j++) {
        System.out.println(array[i]);
    }
}
```

![Time Complexity](images/time-complexity.png)

[Top](#notes-about-algorithms)

### How to measure the codes using Big O

| N | Description   | Complexity    |
|---|---------------|---------------|
| 1 | Any assignment statements and if statements that are executed once regardless of the size of the problem  | O(1)  |
| 2 | A simple "for" loop from 0 to n (with no internal loops)                  | O(n)      |
| 3 | A nested loop of the same type takes quadratic time complexity            | O(n^2)    |
| 4 | A loop, in which the controlling parameter is divided by two at each step | O(log n)  |
| 5 | When dealing with multiple statements, just add them up                   |           |
