package learn.algo.mc.s07arrays;

import java.util.HashSet;

public class P012ContainsDuplicateLC0217 {

    public static boolean containsDuplicate(int[] nums) {
        HashSet<Integer> set = new HashSet<>();
        for (int n : nums) {
            if (!set.add(n)) {
                return true;
            }
        }
        return false;
    }
}
