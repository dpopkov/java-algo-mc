package learn.algo.mc.s07arrays;

import java.util.HashMap;
import java.util.Map;

/*
Find a pair of indexes pointing to integers whose sum is equal to a given number.
 */
public class P011TwoSumLC0001 {

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int diff = target - nums[i];
            Integer foundIdx = map.get(diff);
            if (foundIdx != null) {
                return new int[]{foundIdx, i};
            } else {
                map.put(nums[i], i);
            }
        }
        return new int[0];
    }
}
