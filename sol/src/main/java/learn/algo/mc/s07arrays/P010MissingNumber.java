package learn.algo.mc.s07arrays;

/*
Find the missing number in an integer array of 1 to 10.
 */
public class P010MissingNumber {

    public static int missingNumber(int[] array) {
        int expectedSum = 10 * (10 + 1) / 2;
        int actualSum = 0;
        for (int n : array) {
            actualSum += n;
        }
        return expectedSum - actualSum;
    }
}
