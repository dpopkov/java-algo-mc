package learn.algo.mc.s07arrays;

import java.util.Arrays;

/*
Implement two dimensional array.
 */
public class P009Int2dArray {

    static final int EMPTY_CELL_VALUE = Integer.MIN_VALUE;

    private final int[][] array;
    private final int height;
    private final int width;

    public P009Int2dArray(int numRows, int numColumns) {
        this.height = numRows;
        this.width = numColumns;
        array = new int[numRows][numColumns];
        for (int[] row : array) {
            Arrays.fill(row, EMPTY_CELL_VALUE);
        }
    }

    public boolean isEmpty(int row, int column) {
        validateIndexes(row, column);
        return array[row][column] == EMPTY_CELL_VALUE;
    }

    public void insert(int row, int column, int value) {
        if (value == EMPTY_CELL_VALUE) {
            throw new IllegalArgumentException(String.format("The value %d is not allowed", value));
        }
        validateIndexes(row, column);

        if (array[row][column] == EMPTY_CELL_VALUE) {
            array[row][column] = value;
        } else {
            throw new IllegalArgumentException(String.format("The cell is occupied at position [%d,%d]", row, column));
        }
    }

    public int get(int row, int column) {
        validateIndexes(row, column);
        return array[row][column];
    }

    private void validateIndexes(int row, int column) {
        if (row < 0 || row >= height) {
            throw new ArrayIndexOutOfBoundsException(
                    String.format("Row index %d is out of bounds for height %d", row, array.length));
        }
        if (column < 0 || column >= width) {
            throw new ArrayIndexOutOfBoundsException(
                    String.format("Column index %d if out of bounds for width %d", column, array[0].length));
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int[] row : array) {
            sb.append(Arrays.toString(row)).append("\n");
        }
        return sb.toString();
    }

    public Position search(int value) {
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if (array[row][col] == value) {
                    return new Position(row, col);
                }
            }
        }
        return Position.NOT_EXISTING;
    }

    public static class Position {
        public static final Position NOT_EXISTING = new Position(-1, -1);

        public final int row;
        public final int column;

        public Position(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public boolean valid() {
            return row != -1 && column != -1;
        }
    }

    public void delete(int row, int column) {
        validateIndexes(row, column);
        array[row][column] = EMPTY_CELL_VALUE;
    }
}
