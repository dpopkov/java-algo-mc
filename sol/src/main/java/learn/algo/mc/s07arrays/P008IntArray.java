package learn.algo.mc.s07arrays;

import java.util.Arrays;
import java.util.function.IntConsumer;

public class P008IntArray {

    static final int EMPTY_CELL_VALUE = Integer.MIN_VALUE;

    private final int[] array;

    public P008IntArray(int size) {
        array = new int[size];
        Arrays.fill(array, EMPTY_CELL_VALUE);
    }

    public void set(int index, int value) {
        if (array[index] != EMPTY_CELL_VALUE) {
            throw new IllegalArgumentException("The cell is occupied at index " + index);
        }
        if (value == EMPTY_CELL_VALUE) {
            throw new IllegalArgumentException("Cannot set value " + EMPTY_CELL_VALUE);
        }
        array[index] = value;
    }

    public int get(int index) {
        return array[index];
    }

    public void traverse(IntConsumer consumer) {
        for (int value : array) {
            if (value != EMPTY_CELL_VALUE) {
                consumer.accept(value);
            }
        }
    }

    public int find(int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
        return -1;
    }

    public void deleteAt(int index) {
        array[index] = EMPTY_CELL_VALUE;
    }

    public boolean isEmptyAt(int index) {
        return array[index] == EMPTY_CELL_VALUE;
    }
}
