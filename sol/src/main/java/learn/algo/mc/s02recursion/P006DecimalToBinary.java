package learn.algo.mc.s02recursion;

/*
How to convert a number from Decimal to Binary using recursion?
 */
public class P006DecimalToBinary {

    static int convert(int x) {
        if (x == 0) {
            return 0;
        }
        return convert(x / 2) * 10 + x % 2;
    }
}
