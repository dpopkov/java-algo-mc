package learn.algo.mc.s02recursion;

/*
How fo find GCD (Greatest Common Divisor) of two numbers using recursion?
GCD is the largest positive integer that divides the numbers without a remainder.
 */
public class P005GCD {
    static int calculate(int a, int b) {
        if (a < b) {
            return calculate(b, a);
        }
        int remainder = a % b;
        if (remainder == 0) {
            return b;
        }
        return calculate(b, remainder);
    }
}
