package learn.algo.mc.s02recursion;

/*
Fibonacci numbers
- Fibonacci sequence is a sequence of numbers in which each number is the sum
of the two preceding ones and the sequence starts from 0 and 1.

0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 */
public class P002Fibonacci {

    static int calculate(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Argument must be non-negative");
        }
        if (n == 0 || n == 1) {
            return n;
        }
        return calculate(n - 2) + calculate(n - 1);
    }
}
