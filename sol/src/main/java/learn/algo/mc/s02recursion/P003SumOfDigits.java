package learn.algo.mc.s02recursion;

/*
How to find the sum of digits of a positive integer number using recursion?
 */
public class P003SumOfDigits {

    static int calculate(int x) {
        if (x < 0) {
            throw new IllegalArgumentException("Argument must be non-negative");
        }
        return calculateRecursively(x);
    }

    private static int calculateRecursively(int x) {
        if (x < 10) {
            return x;
        }
        int digit = x % 10;
        return digit + calculateRecursively(x / 10);
    }
}
