package learn.algo.mc.s02recursion;

/*
How to calculate power of a number using recursion?
 */

public class P004Power {

    static int calculate(int base, int exponent) {
        if (exponent < 0) {
            throw new IllegalArgumentException();
        }
        return powerRecursive(base, exponent);
    }

    private static int powerRecursive(int base, int exponent) {
        if (exponent == 0) {
            return 1;
        } else if (exponent == 1) {
            return base;
        } else if ((exponent & 1) == 0) {
            int m = powerRecursive(base, exponent / 2);
            return m * m;
        }
        return base * powerRecursive(base, exponent - 1);
    }
}
