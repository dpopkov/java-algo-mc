package learn.algo.mc.s02recursion;

public class P007RecursiveProblems {

    /*
    Write a recursive function which accepts a string and returns a new string in reverse.
     */
    static String reverse(String s) {
        if (s.length() < 2) {
            return s;
        }
        return reverse(s.substring(1)) + s.charAt(0);
    }

    /*
    Write a recursive function called which returns true if the string passed to it is a palindrome
    (reads the same forward and backward). Otherwise it returns false.
     */
    static boolean isPalindrome(String s) {
        if (s.length() < 2) {
            return true;
        }
        if (s.charAt(0) == s.charAt(s.length() - 1)) {
            return isPalindrome(s.substring(1, s.length() - 1));
        }
        return false;
    }

    /*
    Write a recursive function called someRecursive which accepts an array and a callback.
    The function returns true if a single value in the array returns true when passed to the callback.
    Otherwise it returns false.
     */
    static boolean someRecursive(int[] arr, OddFunction odd) {
        return someRecursiveFrom(arr, 0, odd);
    }

    private static boolean someRecursiveFrom(int[] arr, int start, OddFunction odd) {
        if (start == arr.length) {
            return false;
        } else if (odd.run(arr[start])) {
            return true;
        }
        return someRecursiveFrom(arr, start + 1, odd);
    }

    static class OddFunction {
        boolean run(int num) {
            return num % 2 != 0;
        }
    }

    /*
    Implement a function that capitalizes each word in String.
    Example
        input: i love java
        output : I Love Java
     */

    static String capitalizeWordForthToBack(String s) {
        if (s.isEmpty()) {
            return s;
        }
        int i = 0;
        while (i < s.length() && Character.isAlphabetic(s.charAt(i))) {
            i++;
        }
        String word = s.substring(0, i);
        word = Character.toUpperCase(word.charAt(0)) + word.substring(1);

        int j = i;
        while (j < s.length() && !Character.isAlphabetic(s.charAt(j))) {
            j++;
        }
        String rest = s.substring(j);
        if (rest.isEmpty()) {
            return word;
        }
        return word + " " + capitalizeWordForthToBack(rest);
    }

    public static String capitalizeWordBackToForth(String str) {
        if (str.isEmpty()) {
            return str;
        }
        char lastChar = str.charAt(str.length() - 1);
        if (str.length() == 1) {
            return Character.toString(Character.toUpperCase(lastChar));
        }
        if (str.substring(str.length() - 2, str.length() - 1).equals(" ")) {
            lastChar = Character.toUpperCase(lastChar);
        }
        return capitalizeWordBackToForth(str.substring(0, str.length() - 1)) + lastChar;
    }
}
