package learn.algo.mc.s02recursion;

import learn.algo.mc.utils.StringToIntArrayConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class P007RecursiveProblemsTest {

    @Test
    void reverse() {
        assertEquals("avaj", P007RecursiveProblems.reverse("java"));
        assertEquals("reverse", P007RecursiveProblems.reverse("esrever"));
    }

    @ParameterizedTest
    @CsvSource({
            "awesome,false",
            "foobar,false",
            "tacocat,true",
            "amanaplanacanalpanama,true",
            "amanaplanacanalpandemonium,false"
    })
    void isPalindrome(String input, boolean expected) {
        assertEquals(expected, P007RecursiveProblems.isPalindrome(input));
    }

    @ParameterizedTest
    @CsvSource({
            "1;2;3;4, true",
            "4;6;8;9, true",
            "4;6;8, false",
    })
    void someRecursive(@ConvertWith(StringToIntArrayConverter.class) int[] arr, boolean expected) {
        boolean actual = P007RecursiveProblems.someRecursive(arr, new P007RecursiveProblems.OddFunction());
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource({
            "first,First",
            "first word,First Word",
            "i love java,I Love Java"
    })
    void capitalizeWordForthToBack(String input, String expected) {
        assertEquals(expected, P007RecursiveProblems.capitalizeWordForthToBack(input));
    }

    @ParameterizedTest
    @CsvSource({
            "first,First",
            "first word,First Word",
            "i love java,I Love Java"
    })
    void capitalizeWordBackToForth(String input, String expected) {
        assertEquals(expected, P007RecursiveProblems.capitalizeWordBackToForth(input));
    }
}
