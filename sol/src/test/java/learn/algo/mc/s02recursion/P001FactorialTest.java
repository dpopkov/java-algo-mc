package learn.algo.mc.s02recursion;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class P001FactorialTest {

    @Test
    void calculate() {
        int expected = 24;
        int actual = P001Factorial.calculate(4);
        assertEquals(expected, actual);
    }

    @Test
    void calculateNegativeArgument_thenException() {
        assertThrows(IllegalArgumentException.class,
                () -> P001Factorial.calculate(-1));
    }
}
