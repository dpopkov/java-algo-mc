package learn.algo.mc.s02recursion;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class P002FibonacciTest {

    @ParameterizedTest
    @CsvSource({"0,0", "1,1", "2,1", "3,2", "9,34"})
    void calculate(int n, int expected) {
        assertEquals(expected, P002Fibonacci.calculate(n));
    }

    @Test
    void calculateNegativeArgument_thenException() {
        assertThrows(IllegalArgumentException.class,
                () -> P002Fibonacci.calculate(-1));
    }
}
