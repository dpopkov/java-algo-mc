package learn.algo.mc.s02recursion;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class P006DecimalToBinaryTest {

    @ParameterizedTest
    @CsvSource({
            "0,0",
            "1,1",
            "2,10",
            "3,11",
            "5,101",
            "13,1101"
    })
    void convert(int input, int expected) {
        assertEquals(expected, P006DecimalToBinary.convert(input));
    }
}
