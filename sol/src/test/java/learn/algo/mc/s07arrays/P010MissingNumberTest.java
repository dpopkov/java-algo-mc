package learn.algo.mc.s07arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class P010MissingNumberTest {

    @Test
    void missingNumber() {
        int[] array = {1, 2, 3, 4, 5, 6, 8, 9, 10};
        int actual = P010MissingNumber.missingNumber(array);
        assertEquals(7, actual);
    }
}
