package learn.algo.mc.s07arrays;

import org.junit.jupiter.api.Test;

import static learn.algo.mc.s07arrays.P009Int2dArray.*;
import static org.junit.jupiter.api.Assertions.*;

class P009Int2dArrayTest {

    private final P009Int2dArray array = new P009Int2dArray(2, 2);

    @Test
    void testInsertThenGet() {
        final int value = 42;
        assertTrue(array.isEmpty(0,0));
        array.insert(0, 0, value);
        assertFalse(array.isEmpty(0,0));
        int actual = array.get(0, 0);
        assertEquals(value, actual);
        array.insert(0, 1, value);
        actual = array.get(0, 1);
        assertEquals(value, actual);
    }

    @Test
    void testInsertWhenOccupied_thenException() {
        final int value = 42;
        array.insert(0, 0, value);
        assertThrows(IllegalArgumentException.class,
                () -> array.insert(0, 0, value));
    }

    @Test
    void testInsertInvalidValue_thenException() {
        assertThrows(IllegalArgumentException.class,
                () -> array.insert(0, 0, EMPTY_CELL_VALUE));
    }

    @Test
    void testInsertBeyondRowLimits_thenException() {
        assertThrows(ArrayIndexOutOfBoundsException.class,
                () -> array.get(2, 1));
    }

    @Test
    void testInsertBeyondColumnLimits_thenException() {
        assertThrows(ArrayIndexOutOfBoundsException.class,
                () -> array.get(1, 2));
    }

    @Test
    void testSearch() {
        array.insert(0, 0, 11);
        array.insert(1, 1, 22);
        Position pos11 = array.search(11);
        Position pos22 = array.search(22);
        Position pos33 = array.search(33);
        assertTrue(pos11.valid());
        assertEquals(0, pos11.row);
        assertEquals(0, pos11.column);
        assertTrue(pos22.valid());
        assertEquals(1, pos22.row);
        assertEquals(1, pos22.column);
        assertFalse(pos33.valid());
    }

    @Test
    void testDelete() {
        array.insert(0, 0, 11);
        assertEquals(11, array.get(0, 0));
        array.delete(0, 0);
        assertEquals(EMPTY_CELL_VALUE, array.get(0, 0));
    }
}
