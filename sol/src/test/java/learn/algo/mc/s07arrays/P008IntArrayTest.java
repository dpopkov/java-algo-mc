package learn.algo.mc.s07arrays;

import org.junit.jupiter.api.Test;

import java.util.function.IntConsumer;

import static org.junit.jupiter.api.Assertions.*;

class P008IntArrayTest {
    P008IntArray arr = new P008IntArray(4);

    @Test
    void testSet_whenSetIntoEmptyCellThenSuccess() {
        final int value = 42;
        arr.set(0, value);
        assertEquals(value, arr.get(0));
    }

    @Test
    void testSet_whenSetIntoOccupiedCellThenException() {
        final int value = 42;
        arr.set(0, value);
        assertThrows(IllegalArgumentException.class, () -> arr.set(0, value));
    }

    @Test
    void testSet_whenSetIllegalValueThenException() {
        assertThrows(IllegalArgumentException.class, () -> arr.set(0, P008IntArray.EMPTY_CELL_VALUE));
    }

    @Test
    void testTraverse_whenTraversingThenAcceptEveryValue() {
        arr.set(0, 10);
        arr.set(2, 20);
        final int[] values = new int[2];
        IntConsumer consumer = new IntConsumer() {
            private int idx;
            @Override
            public void accept(int value) {
                values[idx++] = value;
            }
        };
        arr.traverse(consumer);
        assertEquals(10, values[0]);
        assertEquals(20, values[1]);
    }

    @Test
    void testFind_whenFindPresentElementThenReturnIndex() {
        arr.set(0, 10);
        arr.set(1, 20);
        assertEquals(0, arr.find(10));
        assertEquals(1, arr.find(20));
    }

    @Test
    void testFind_whenFindNonPresentElementThenReturnMinusOne() {
        assertEquals(-1, arr.find(10));
    }

    @Test
    void testDelete() {
        final int index = 0;
        arr.set(index, 10);
        assertEquals(10, arr.get(index));
        assertFalse(arr.isEmptyAt(index));
        arr.deleteAt(index);
        assertTrue(arr.isEmptyAt(index));
    }
}
